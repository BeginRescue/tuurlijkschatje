var app = {
    initialize: function() {
        this.bindEvents();
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("resume", this.onDeviceReady, false);
    },

    onDeviceReady: function() {
        FastClick.attach(document.body);
        StatusBar.hide();
        app.loadData();
        var introductionSwiper = new Swiper('#introduction_swiper', { pagination: '.swiper-pagination' });
        var articleSwiper = new Swiper('#article_swiper', { initialSlide: app.getSlideNumber() } );
    },

    getSlideNumber: function() {
        var type = new Date().getDay();
        
        var calculatedDate = new Date(window.localStorage.getItem("calculatedDate"));

        var days = app.daysFromNow(calculatedDate);                    

        var substractDay = (calculatedDate.getDay() == 0) ? 7 : calculatedDate.getDay();
        var calculatedDay = 280 - days + substractDay - 7;

        var slide = calculatedDay - 1;

        return slide;
    },

    buildMessages: function(data) {
        var weeks = _.sortBy(data.weeks, "number");
        var messages = [];

        _.each(weeks, function(week, key) {
            var posts = _.sortBy(week.posts, "type")
            _.each(posts, function(post, key) {
                messages.push({
                    week: week.title,
                    number: week.number,
                    title: post.title,
                    body: post.body,
                    type: post.type
                })
            }); 
        });

        window.localStorage.setItem("messages", JSON.stringify(messages));
    },

    loadInitialData: function() {
        $.ajax({
            url: 'data.json',
            dataType: 'json',
            async: false,
            success: function(data) {
                app.buildMessages(data);
            }
        });
    },

    updateData: function() {
        var version = window.localStorage.getItem("dataVersion");
        if (!version) {
            window.localStorage.setItem("dataVersion", 1);
        }
        version = window.localStorage.getItem("dataVersion");
        $.ajax({
          url: 'http://api.tuurlijkschatje.nl/v1/data',
          dataType: 'json',
          timeout: 1500,
          success: function(data) {
            if (data.version > version) {
                console.log("Updated!")
                app.buildMessages(data);
                window.localStorage.setItem("dataVersion", data.version);
            }
          }
        });
    },

    loadData: function() {
        var messages = window.localStorage.getItem("messages");
        if (messages) {
            app.updateData();
        } else {
            console.log("Load initial data")
            app.loadInitialData();
        }

        var messages = JSON.parse(window.localStorage.getItem("messages"));
        var calculatedDate = window.localStorage.getItem("calculatedDate");

        // Only if calculated date entered
        if (calculatedDate) {
            var parsedDate = new Date(calculatedDate);
            var days = app.daysFromNow(parsedDate);
            // Check if not more than 40 weeks. Impossible.
            if (days < 280) {
                if (parsedDate) {
                    var slides = 0;
                    var maxSlides = app.getSlideNumber();

                    $("#article_swiper .swiper-wrapper").empty();
                    _.each(messages, function(message, key) {
                        if (slides <= maxSlides) {
                            var postHtml = '<div class="swiper-slide"><div class="swiper-slide-wrapper"><div class="swiper-slide-content"><h1>' + message.title + '</h1><div class="description-header"><img src="img/type/' + message.type + '.png" class="type-icon"><span class="description-header-content">' + message.week + '</span><img src="img/weeknumber/' + message.number + '.png" class="weeknumber-icon"></div><div class="article_content">' + message.body + '</div></div></div></div>';
                            $("#article_section .swiper-wrapper").append(postHtml);
                            slides++;
                        }
                    }); 
            }
            } else {
                var postHtml = '<div class="swiper-slide"><div class="swiper-slide-wrapper"><div class="swiper-slide-content"><div class="article_content">Wow, dat is knap werk.<br>Je kunt gewoon voorspellen dat je zwanger gáát worden!<br>Of je hebt per ongeluk een datum ingevuld<br>die verder weg ligt dan 40 weken.<br>Dat kan ook.<br>Wist je trouwens dat de Afrikaanse olifant de langste draagtijd heeft?<br>Liefst 645 dagen.<br>Geen wonder dat de mannetjes een dikke huid hebben...</div></div></div></div>';
                $("#article_section .swiper-wrapper").append(postHtml);
            }
        };

        // Show settings icon, because ugly preload
        $('.settings-icon').show();
    },

    updateCalculatedDate: function(calculatedDate) {
        window.localStorage.setItem("calculatedDate", calculatedDate);
    },

    updatePushTime: function(pushTime) {
        window.localStorage.setItem("pushTime", pushTime);
    },

    updatePushEnabled: function(pushEnabled) {
        window.localStorage.setItem("pushEnabled", pushEnabled);
    },

    daysFromNow: function(b) {
        var a = new Date();

        var _MS_PER_DAY = 1000 * 60 * 60 * 24;

        // a and b are javascript Date objects
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);

    }
};